# ==============================================================================
# Copyright (C) 2024 Pavol Federl pfederl@ucalgary.ca
# Do not distribute this file.
# ==============================================================================
# This is the only file you need to modify and submit for grading.
#
# You can use the incomplete code below as a starting point for the assignment.
# The only function you need to implement is fwsim() and it needs to have
# the same signature as below. You can delete all other code.
#
# The code below does not work for most packets/rules. It is also inefficient.
# And it does not handle blank lines, nor comment lines. I included it it
# mostly to illustrate how to return results and errors from the fwsim()
# function.


def find_match(rule_lines: list[str], packet: str, rule_fname: str) -> int:
    """helper function - feel free to delete it, 
    it does not work very well anyways"""
    pfields = packet.split()
    for line_no, rule in enumerate(rule_lines, 1):
        rfields = rule.split()
        if len(rfields) not in [4, 5]:
            raise Warning(
                f"{rule_fname}:{line_no}: rule must have 4 or 5 fields")
        iprange = rfields[2].split("/")[0]
        if pfields[0] == rfields[0] and (pfields[1] == iprange or iprange == '*'):
            return line_no
    return 0


def fwsim(rules_fname: str, packets_fname: str) -> list[list[str]]:
    """this is the function you need to implement

    Returns a list of results, where each result itself is a list representing
    the the result of matching the packet to a rule.
    For each packet, insert a list [action, rule_line, direction, ip, port, flag]
    into the result. These fields are described in the assignment specification
    """

    try:
        rules = open(rules_fname, "r").readlines()
        packets = open(packets_fname, "r").readlines()
    except Exception as e:
        raise Warning(
            f"failed to read rules from '{rules_fname}', "
            f"or packets from '{packets_fname}': {e}")

    # super inefficient implementation below
    # it requires re-parsing of rules for every packet!!!
    results = []
    for pline, packet in enumerate(packets, 1):
        pfields = packet.split()
        if len(pfields) != 4:
            raise Warning(f"{packets_fname}:{pline}: packet needs 4 fields")
        rline = find_match(rules, packet, rules_fname)
        if rline == 0:
            results.append(["default", ""] + pfields)
        else:
            results.append([rules[rline-1].split()[1], str(rline)] + pfields)
    return results
